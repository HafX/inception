﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AvatarController))]
public class Player : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 6;
    [SerializeField] private float maxJumpHeight = 4;
    [SerializeField] private float minJumpHeight = 1;
    [SerializeField] private float timeToJumpApex = .4f;
    [SerializeField] private float accelerationTimeAirborne = 0f;
    [SerializeField] private float accelerationTimeGrounded = 0f;

    private float gravity;
    private float maxJumpVelocity;
    private float minJumpVelocity;
    private Vector3 velocity;
    private float velocityXSmoothing;

    private Vector2 spawnPos;

    private AvatarController controller;

    void Start()
    {
        controller = GetComponent<AvatarController>();

        spawnPos = transform.position;
        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
    }

    void Update()
    {
            if (controller.collisions.above || controller.collisions.below)
            {
                velocity.y = 0;
            }

        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if ((JumpInputDown()) && (controller.collisions.below || controller.PowerUpJumping))
        {
            velocity.y = maxJumpVelocity;
        }

        if (JumpInputUp())
        {
            if (velocity.y > minJumpVelocity)
            {
                velocity.y = minJumpVelocity;
            }
        }

        float targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, 
            (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }

    private static bool JumpInputDown()
    {
        return Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.Space);
    }

    private static bool JumpInputUp()
    {
        return Input.GetKeyUp(KeyCode.Joystick1Button0) || Input.GetKeyUp(KeyCode.Space);
    }

    private void OnBecameInvisible()
    {
        transform.position = spawnPos;
    }
}